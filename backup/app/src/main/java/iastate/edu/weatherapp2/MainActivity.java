package iastate.edu.weatherapp2;

import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private String location = "Brooklyn";
    private String requestFormat = "https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=YOUR_API_KEY";
    private String request0 = "https://api.openweathermap.org/data/2.5/weather?q=";
    private String request1 = "&appid=61fc05035b94d0c8fa7db3dee110beb2";
    private String completeRequest = request0+location+request1;


    //r0 + location + r1
    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        final TextView t0 = findViewById(R.id.text0);
        t0.setText(location);
        setSupportActionBar(toolbar);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, completeRequest,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try{
                            JSONObject test = new JSONObject(response);
                            JSONObject main = test.getJSONObject("main");
                            String temperature = main.getString("temp");
                            double temperatureF = Double.parseDouble(temperature);
                            temperatureF = (temperatureF - 273.15)*(9/5)+32;
                            String temperatureW = ""+temperatureF;
                            t0.setText(temperatureW);

                        }catch(JSONException e)
                        {

                        }
//                      String temperature = main.getString("temp");
                       // t0.setText("Response is: "+ response.substring(0,10));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("log2=", error.toString());
                Log.e("log3=", error.toString());
                t0.setText("That didn't work!");
            }
        });

        queue.add(stringRequest);

//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
//                (Request.Method.GET, completeRequest, null, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                             JSONObject main = response.getJSONObject("main");
//                             String temperature = main.getString("temp");
//                             t0.setText(temperature);
//
//                            }catch(JSONException e){
//
//                            }
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                });




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
