package iastate.edu.weatherapp2;

import android.app.ActivityOptions;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.core.view.MotionEventCompat;
import androidx.preference.PreferenceManager;

import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.TextView;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private NotificationManagerCompat notificationManager;

    private String location = "Brooklyn";
    private boolean units = false;
    private String temperaturePrinted;
    private String DEBUG_TAG = "log=";
   // private String requestFormat = "https://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=YOUR_API_KEY";
    private String request0 = "https://api.openweathermap.org/data/2.5/weather?q=";
    private String request1 = "&appid=61fc05035b94d0c8fa7db3dee110beb2";
    private String completeRequest = request0+location+request1;
    private String imageRequest0 = "http://openweathermap.org/img/wn/";
    private String imageRequest1 = "@2x.png";
    private Context context;
    private String completeImageRequest;
    private FusedLocationProviderClient fusedLocationClient;
    private Location testLoc;
    private LocationManager locationManager;
    private double currentLatitude,currentLongitude;
    private GestureDetectorCompat mDetector;


    TextView t0;
    ImageView img_weather;
    TextView txt_city_name, txt_humidity,txt_sunrise,txt_sunset,txt_pressure,txt_description,txt_date_time,txt_wind;



    //r0 + location + r1
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        notificationManager = NotificationManagerCompat.from(this);




        super.onCreate(savedInstanceState);
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        setContentView(R.layout.activity_main);
        mDetector = new GestureDetectorCompat(this,new MyGestureListener()  );
        Toolbar toolbar = findViewById(R.id.toolbar);
        t0 = findViewById(R.id.text0);
        setSupportActionBar(toolbar);

        txt_city_name = findViewById(R.id.tex_city_name);
        txt_humidity = findViewById(R.id.txt_humidity);
        txt_sunrise = findViewById(R.id.txt_sunrise);
        txt_sunset = findViewById(R.id.txt_sunset);
        txt_pressure = findViewById(R.id.txt_pressure);
        txt_description = findViewById(R.id.txt_description);
        txt_date_time = findViewById(R.id.txt_date_time);
        img_weather = findViewById(R.id.img_weather);
        //txt_wind = findViewById(R.id.txt_wind);


        context = this;
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = shared.edit();
        location = shared.getString("location_input","Brooklyn");
        units = shared.getBoolean("switch",false);
        completeRequest = request0+location+request1;
        t0.setText(location);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, completeRequest,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try{
                            JSONObject test = new JSONObject(response);
                            JSONObject main = test.getJSONObject("main");
                            JSONObject sys = test.getJSONObject("sys");
                            JSONObject wind = test.getJSONObject("wind");
                            JSONArray weather = test.getJSONArray("weather");
                            JSONObject weatherobj = weather.getJSONObject(0);
                            //Temperature
                            String temperature = main.getString("temp");
                            double temperatureK = Double.parseDouble(temperature);

                            //City Name
                            String city_name = test.getString("name");
                            String condition = weatherobj.getString("description") + " in ";
                            txt_city_name.setText(city_name);
                            txt_description.setText(new StringBuilder(condition).append(test.getString("name")));

                            //Date
                            int dt = test.getInt("dt");
                            Date date = new Date(dt*1000L);
                            SimpleDateFormat simpleDate =  new SimpleDateFormat("HH:mm EEE MM/dd/yyyy");
                            String formattedDate = simpleDate.format(date);
                            txt_date_time.setText(formattedDate);

                            //Pressure
                            txt_pressure.setText(new StringBuilder(main.getString("pressure")).append("hpa"));

                            //Humidity
                            txt_humidity.setText(new StringBuilder(main.getString("humidity")).append("%"));

                            //Sunrise && Sunset
                            int sr = sys.getInt("sunrise");
                            int ss = sys.getInt("sunset");
                            Date srd = new Date(sr*1000L);
                            Date ssd = new Date(ss*1000L);
                            SimpleDateFormat simpleDate2 =  new SimpleDateFormat("HH:mm");
                            String formattedSunrise = simpleDate2.format(srd);
                            String formattedSunset = simpleDate2.format(ssd);
                            txt_sunrise.setText(formattedSunrise);
                            txt_sunrise.setText(formattedSunset);

                            //Image
                            String condition_icon = weatherobj.getString("icon");

                            completeImageRequest = imageRequest0+condition_icon+imageRequest1;
                            new imgJob().execute(completeImageRequest);




                            // Wind
//                            double speed = wind.getDouble("speed");
//                            double deg = wind.getDouble("deg");
//                            txt_wind.setText(new StringBuilder("speed:").append(speed).append("deg:").append(deg));


                            if(units == false) {
                                int temperatureF = ConvertKelvinToF(temperatureK);
                                temperaturePrinted = "" + temperatureF + "\u00B0"+"F";
                                if(temperatureF > 90){
                                    setNotification("Extreme Heat Warning");
                                }
                                if(temperatureF < 32){
                                    setNotification("Extreme Cold Warning");
                                }
                            }
                            else
                            {
                                int temperatureC = ConvertKelvinToC(temperatureK);
                                temperaturePrinted = ""+temperatureC + "\u00B0"+"C";
                                if(temperatureC > 32)
                                {
                                    setNotification("Extreme Heat Warning");
                                }
                                if(temperatureC < 0 )
                                {
                                    setNotification("Extreme Cold Warning");
                                }
                            }
                            t0.setText(temperaturePrinted);


                        }catch(JSONException e)

                        {

                        }
//                      String temperature = main.getString("temp");
                       // t0.setText("Response is: "+ response.substring(0,10));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("log2=", error.toString());
                Log.e("log3=", error.toString());
                t0.setText("That didn't work!");


            }
        });

        queue.add(stringRequest);

        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        try{
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,0,this);
        }catch(SecurityException e)
        {

        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            getWindow().setExitTransition(new Explode());
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
            return true;
        }
        if(id == R.id.action_GPS){
            Geocoder gcd = new Geocoder(this,Locale.getDefault());
            SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
            final SharedPreferences.Editor editor = shared.edit();

            try {
                List<Address> addresses = gcd.getFromLocation(currentLatitude, currentLongitude, 1);
                String currentLocation = addresses.get(0).getLocality();
                editor.putString("location_input",currentLocation);
                editor.commit();
                getWindow().setExitTransition(new Fade());
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
            }catch(IOException e)
            {

            }


        }

        return super.onOptionsItemSelected(item);
    }

    public int ConvertKelvinToF(double kelvin)
    {
        double fahrenheit;
        fahrenheit = 1.8*(kelvin - 273.16)+32;
        int fahrenheitRounded = (int) fahrenheit;
        return fahrenheitRounded;
    }
    public int ConvertKelvinToC(double kelvin)
    {
        double celsius = kelvin - 273.16;
        int celsiusRounded = (int) celsius;
        return celsiusRounded;
    }

    public static Drawable LoadImageFromWeb(String url){
        try {


            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is,"src");
            return d;
        }catch(Exception e){
            return null;
        }
    }
    private class imgJob extends AsyncTask<String,Void,String> {
        Drawable d;
        @Override
        protected String doInBackground(String[] params){
            d = LoadImageFromWeb(params[0]);
            return "";
        }

        @Override
        protected void onPostExecute(String message){
            img_weather.setImageDrawable(d);
        }
    }

    public void setNotification(String condition) {
        Notification notification = new NotificationCompat.Builder(this, ExtremeNotification.Notification)
        .setSmallIcon(R.drawable.ic_one)
        .setContentTitle("Extreme Weather Alarm")
        .setContentText(condition)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setCategory(NotificationCompat.CATEGORY_ALARM)
        .build();

        notificationManager.notify(1,notification);
    }

    public void onLocationChanged(Location location){
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
    }


    public void onProviderEnabled(String provider){}
    public void onProviderDisabled(String provider) {}
    public void onStatusChanged(String provider, int status, Bundle extras){}



    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent event) {
          //  Log.d(DEBUG_TAG,"onDown: " + event.toString());
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {

            boolean result = false;
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                        } else {
                            onSwipeLeft();
                        }
                        result = true;
                    }
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }
                    result = true;
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return result;
        }
    }

    public void onSwipeRight(){

    }

    public void onSwipeLeft(){
        getWindow().setExitTransition(new Fade());
        Intent intent = new Intent(this,ForecastActivity.class);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

    }

    public void onSwipeBottom(){

    }
    public void onSwipeTop(){

    }
}
